This website by [The Codeberg Documentation Contributors](CONTRIBUTORS.md)
is licensed under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en).

Due to the wiki-like nature of this website, pages usually have multiple authors and are changed
frequently. Changes to the original versions of the works as well as their individual authors can be
looked up in [the website's commit history](https://codeberg.org/Codeberg/Documentation/commits/branch/master).

The Codeberg logos in this website are by mray,
licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/deed.en).

Codeberg and the Codeberg Logo are trademarks of Codeberg e.V.

"Knut the Polar Bear" has been derived from https://openclipart.org/detail/193243/polar-bear-remix, under CC0 1.0

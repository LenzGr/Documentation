---
eleventyNavigation:
  key: InstallGit
  title: Install Git
  parent: GettingStarted
  order: 25
---

Projects on Codeberg use [Git](https://git-scm.com/) as their [version control](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control) system. If you want to contribute source code, it is a good idea to install Git at this point.

Git runs on every OS. You can download it directly from the [Git website](https://git-scm.com/downloads), or from your operating system’s package manager (if available).

Here we provide step-by-step instructions to install it.

**Disclaimer**: we try to keep these instructions up-to-date, but you might need to adjust them to your machine/OS.

## Linux
On Linux, you can install Git directly from your package manager. Check out [Git's official documentation](https://git-scm.com/download/linux) for the right command for your distro.

Please see also the article [Configuring Git](/git/configuring-git), which describes how to set up your name and email address for Git.

## macOS
There are [several possibilities](https://git-scm.com/download/mac) to install Git on macOS. This guide will show you how to install Git using the Homebrew package manager. You will need to be on macOS High Sierra (10.13) or higher.

### Install
You will first need to install Homebrew. You can do this following the instructions on their [website](https://brew.sh/).

Once you've installed Homebrew, you can install Git from your terminal using the following command.
```bash
% brew install git
```
### Update
To update Git, run the following command.
```bash
% brew upgrade git
```

### Configure
You now managed to get Git up and running. Please proceed as described in the article [Configuring Git](/git/configuring-git) to set up your name and email address for Git.

## Windows 10
There are several possibilities to install Git on Windows. An easy one is to use Git’s install wizard, shown here.

### Admin or user?
For some Git clients (see [below](#git-clients)), e.g. [RStudio](https://rstudio.com/), it is recommended to install Git as administrator on your machine. It is also possible to install as regular user, but you might have to manually specify the path to the Git executable in the settings of your Git client.

### Install
If you want to install as regular user, just log in with your user account and run the installer.
To install with administrator rights, either log in as administrator, or run the installer as administrator. In the latter case, right-click on the installer, select `Run as administrator` and enter your credentials:

<picture>
  <source srcset="/assets/images/getting-started/install-git/win-run-as-admin.webp" type="image/webp">
  <img src="/assets/images/getting-started/install-git/win-run-as-admin.png" alt="win-run-admin">
</picture>


From there, follow the steps in the setup wizard. It is okay to just accept the defaults, when in doubt.  
There are some steps for which you might have to pay attention:

1. Choose your editor:
By default, [Vim](https://www.vim.org/) will be used for Git. But if you prefer to use another one, select it here.

<picture>
  <source srcset="/assets/images/getting-started/install-git/win-editor.webp" type="image/webp">
  <img src="/assets/images/getting-started/install-git/win-editor.png" alt="win-editor">
</picture>

2. PATH environment:
This makes sure that Git can be used by 3rd party software ([Git clients](#git-clients)). It is recommended (and the default anyway).

<picture>
  <source srcset="/assets/images/getting-started/install-git/win-path.webp" type="image/webp">
  <img src="/assets/images/getting-started/install-git/win-path.png" alt="win-path">
</picture>

3. Choose credential helper:  
From Git 2.29.0 onwards, the `Git Credential Manager Core` will be used by default. The `Git Credential Manager` can still be installed and used, but is now deprecated. When using either of these credential managers, your Git/Codeberg credentials will be stored in Windows' [Credential Manager](https://support.microsoft.com/en-us/help/4026814/windows-accessing-credential-manager) when you enter them for your first pull/push (see [Clone & Commit via HTTP](/git/clone-commit-via-http)). Windows' Credential Manager is also where you have to go in order to change them. Follow the links on the installation wizard for more details.  
If you prefer not to store your credentials in Windows but e.g. in a password manager, select the third option (`None`). You will have to enter your credentials at every push/pull.

<picture>
  <source srcset="/assets/images/getting-started/install-git/win-cred-helper.webp" type="image/webp">
  <img src="/assets/images/getting-started/install-git/win-cred-helper.png" alt="win-cred-helper">
</picture>

> You may get the error message displayed below, that the release notes cannot be displayed. That error message can be safely ignored. You can find the release notes in `Start Menu > Git > Git Release Notes`.
>
> <picture>
>   <source srcset="/assets/images/getting-started/install-git/win-error-notes.webp" type="image/webp">
>   <img src="/assets/images/getting-started/install-git/win-error-notes.PNG" alt="win-error">
> </picture>

### Run
Git can be used through the `Command Prompt` (the traditional Windows command line interpreter), the `Windows Powershell` (the "replacement" of the `Command Prompt`), the interface of your [Git client](#git-clients), or `Git Bash`. The latter two options are recommended.

`Git Bash` is what comes with your Git installation. You can access it from the Start Menu: `Git > Git Bash`.

### Configure
You now managed to get Git up and running. Please proceed as described in the article [Configuring Git](/git/configuring-git) to set up your name and email address for Git.

### Update
If you ticked the box to check for updates during installation, Git will inform you that a new version is available.  
To check for updates, at the command line, type:
```bash
git update-git-for-windows
```
Then, just follow the instructions of the terminal to download and install the latest version.

Alternatively, you can also download the installer from the Git website as explained above.
> Make sure that you run the new installer with the same rights (administrator or user) as for the original installation. If not, it will be installed twice and that will create a mess.

If you want to keep all your previous settings, simply tick the box `Only show new options` in the installation wizard (see screenshots above).

<a name="git-clients"></a>
## Git clients
Git can be used from the command line as shown above, but it can also be used through graphical user interfaces called *Git clients*.
You can find a list of some available Git clients on the [Git website](https://git-scm.com/downloads/guis).

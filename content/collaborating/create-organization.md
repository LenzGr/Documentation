---
eleventyNavigation:
  key: CreateOrganization
  title: Create and Manage an Organization
  parent: Collaborating
  order: 40
---

## What is an organization?
An organization is a group of users that have access to a shared account. As such, they can all access the different projects (repositories) of the organization. An organization can be composed of several teams, each having a defined role in the project's development and also different access rights. This makes organzations an easy and powerful tool to collaborate on a project.

Everyone can create organizations on Codeberg for free. The following sections will show you how to create and manage an organization.

> Details about the roles involved (owner, admin, member...) are given in the section [Access rights](#access-rights) below.

<a name="create-orga"></a>
## Create an Organization
On your Dashboard, click on the `+` next to your avatar and select `New Organization`:

<picture>
  <source srcset="/assets/images/collaborating/create-organization/new-orga.webp" type="image/webp">
  <img src="/assets/images/collaborating/create-organization/new-orga.png" alt="new-orga">
</picture>

In the new form, choose a name for your organization (here `PolarClub`) and define its visibility. Finally, choose what the administrators of repositories can do: if the box is ticked, repository administrators will be able to grant any team access to the repository (see [Access rights](#access-rights) below for details on repository administrators).

Confirm by clicking the green `Create Organization`: 

<picture>
  <source srcset="/assets/images/collaborating/create-organization/create-orga.webp" type="image/webp">
  <img src="/assets/images/collaborating/create-organization/create-orga.png" alt="create-orga">
</picture>

You are then directed to the organization's dashboard.

## Switch between your personal account and the organization
On the dashboard, click on the avatar on the left and select the context (personal account or organization) you want to work with. Alternatively, access your repositories or your organization from the tabs on the right side.

<picture>
  <source srcset="/assets/images/collaborating/create-organization/dashboard.webp" type="image/webp">
  <img src="/assets/images/collaborating/create-organization/dashboard.png" alt="dashboard">
</picture>

## View organization
The organization's dashboard gives you an overview of the organization:

<picture>
  <source srcset="/assets/images/collaborating/create-organization/view-orga.webp" type="image/webp">
  <img src="/assets/images/collaborating/create-organization/view-orga.png" alt="view-orga">
</picture>

If you click on the `View [name of organization]` (here `PolarClub`), you will be directed to the organization's page:

<picture>
  <source srcset="/assets/images/collaborating/create-organization/orga.webp" type="image/webp">
  <img src="/assets/images/collaborating/create-organization/orga.png" alt="orga">
</picture>

From there, you can access all repositories (there is none in this example) and create a new one. There is a list of all the members (`People`) and teams; in this example, there is only one member (Knut the polar bear), and one team (`Owners`) with one member (yourself) and no repository. 

## Edit organization
The cog next to the organization's name will lead you to the organization's settings:

<picture>
  <source srcset="/assets/images/collaborating/create-organization/cog.webp" type="image/webp">
  <img src="/assets/images/collaborating/create-organization/cog.png" alt="cog">
</picture>

This is where you can change general settings related to your organization, such as its name, avatar, website or visibility. It is also where you can delete your organization, and access more advanced settings like organization webhooks:

<picture>
  <source srcset="/assets/images/collaborating/create-organization/orga-settings.webp" type="image/webp">
  <img src="/assets/images/collaborating/create-organization/orga-settings.png" alt="orga-settings">
</picture>

The `Name` of the organization is the name that will define the URLs of the organization and of all its repositories; it is recommended to keep it short. This name will also appear on the member's profiles (see [People](#people)). On the other hand, the `Full Name` is the name that will appear on the organization's home page.

In the `Labels` tab, you can create labels that will be used across all repositories of this organization. This will help with issues and pull requests. The default label set is a good starting point.

In the top right corner, you have access to the people (members) and teams of your organization. These can also be accessed from the organization's page.

<a name="teams"></a>
## Teams
The `Teams` tab gives you an overview of the different teams, of their members and of their number of repositories. You can also join a team from there if you have the permission to do so: 

<picture>
  <source srcset="/assets/images/collaborating/create-organization/teams.webp" type="image/webp">
  <img src="/assets/images/collaborating/create-organization/teams.png" alt="teams">
</picture>

When you create an organization, a team `Owners` is created and you are automatically added to this team. Members of the Owners' team have all rights (see [Access rights](#access-rights) below for details).

Click the green `+ New Team` to create a new team. Define its name, permissions and access in the new form:

<picture>
  <source srcset="/assets/images/collaborating/create-organization/new-team.webp" type="image/webp">
  <img src="/assets/images/collaborating/create-organization/new-team.png" alt="new-team">
</picture>

You can choose whether members of the team can only access some repositories explicitly added to the team, or whether they can access all repositories of the organziation.  
You can also allow members to be able to create new repositories for the organization.   
If you have allowed repository administrators to grant or remove access for teams (see [Create an Organization](#create-orga) above), they can do so in `Settings > Collaborators` tab of the repository.  
If you choose either `Read` or `Write` access, you can additionally define which sections of the repositories (code, issues, pull requests, releases and wiki) the members will have (read or write) access to. On the other hand, `Administrator` access automatically grants read and write access to all sections; this part of the form is therefore hidden in this case.  
See the section [Access rights](#access-rights) below for details.

If you belong to the team `Owners`, you can edit a team. For this, go to the `Teams` tab and click on the team you want to edit:

<picture>
  <source srcset="/assets/images/collaborating/create-organization/team-settings.webp" type="image/webp">
  <img src="/assets/images/collaborating/create-organization/team-settings.png" alt="team-settings">
</picture>

Click on `Settings` to edit the team as shown above for the creation of a team.  
This is also where you can add or remove members to a team, and assign repositories to a team.

<a name="people"></a>
## People
On the `People` tab, you can get an overview of all the people who belong to your organization:

<picture>
  <source srcset="/assets/images/collaborating/create-organization/people.webp" type="image/webp">
  <img src="/assets/images/collaborating/create-organization/people.png" alt="people">
</picture>

Somewhat counter-intuitively, this is not where you can add members; this is done in the `Teams` tab (see [Teams](#teams) above).  But you can remove members from the `People` tab.

The visibility of the members can also be edited here: `Hidden` means that the memberships of the members will not be shown on their profiles, while `Visible` makes the avatar of the organization appear in the info card on their profile, as shown in the screenshot below. Note that your membership will always be visible to you on your profile; this visibility setting is for other users only.

<picture>
  <source srcset="/assets/images/collaborating/create-organization/profile.webp" type="image/webp">
  <img src="/assets/images/collaborating/create-organization/profile.png" alt="profile">
</picture>

It is also shown here whether each member has activated the two-factor authentification (`2FA`, see [Setting up Two-factor Authentication](/security/2fa)).  

Finally, you can choose to leave the organization from here.

<a name="access-rights"></a>
## Access rights
An overview of the repository permissions in given in the article [Repository Permissions](/collaborating/repo-permissions).

Members of the "Owners" team can do everything that admins can do. But only owners of the organization can manage the organization, which includes:
- edit/delete the organization,
- add/remove members and teams,
- define access rights of teams,
- edit organization repository settings in the danger zone (i.e. transfer ownership, delete wiki data and repository, and archive repository).

When owners allow members of a team to be able to create new repositories for the organization (see box "Create repositories" in [Teams](#teams) settings), the member who creates the repository will be added as a collaborator with administrator rights to this repository (see [Invite Collaborators](/collaborating/invite-collaborators) for details). 


---
eleventyNavigation:
  key: DocsContributorFAQ
  title: Documentation Contributor FAQ
  parent: ImprovingCodeberg
---

We're very happy you're considering to contribute to Codeberg Documentation!

The intent of this document is to give answers to a number of questions that might arise when starting to contribute to Codeberg Documentation. If your question is not listed, feel free to [ask in the issue tracker](https://codeberg.org/Codeberg/Documentation/issues).

### How do I contribute to Codeberg Documentation?
To contribute to Codeberg Documentation, the first thing you should do (if you haven't already done it) is to create your own fork of the [Codeberg/Documentation repository](https://codeberg.org/Codeberg/Documentation).

Then, for each major contribution that you want to make (e.g. for each new article), create a new branch, make your contributions and finally make a Pull Request to the Codeberg/Documentation repository. You can find the source code to all articles in the `content` directory of Codeberg Documentation's source tree.

The Codeberg Documentation collaborators will then review your pull request, they may request some changes and eventually, once all is good, they may merge your contribution into the official repository and deploy it to the live site.

### How to create a new article?
To write a new article, create a new Markdown file next to the existing files in the section that you want to put your article in. So, if for example you want to create an article called "Keeping your repo clean with .gitignore" in the "Working with Git Repositories" section, put a new file called for example `gitignore.md` into the `content/git` directory. Please adhere to the [file naming conventions](#file-naming-conventions).

Then, add the `eleventyNavigation` header to your newly created file. That header contains information about the article's place in the structure of the site.

It can look like this:

```yaml
---
eleventyNavigation:
  key: WhatIsCodeberg
  title: What is Codeberg?
  parent: GettingStarted
  order: 10
---

## Foo
Hello foo!

## Bar
Hello bar!
```

Every section and article has a unique `key`. If `parent` is set to the `key` of another section, the current article will become a child of that other section.

The other fields are `title` and `order`, where
  
- `title` is merely a display name and determines the menu entry label as well as the article's heading but has no other "technical" semantics and
- `order` is a number that can be used to enforce a specific order of articles (which is good for step-by-step guides such as Getting Started for example).

So to create a new article, find the `key` of your desired parent section in its Markdown source code and use it as the `parent` for your new article.

After the header is in place, you can now start writing your article in Markdown. It is recommended to add a blank line after the header's trailing three dashes.

> Please do not repeat the article's title as a first-level heading in Markdown. It will added automatically when generating the final site.

### Can I preview my article?
Yes, but currently only if you're working with a local clone of the repository. In that case, simply follow the instructions in the [`README.md`](https://codeberg.org/codeberg/documentation/src/branch/master/README.md) file to set up and run a local preview.

It is recommended to set this up, because it can help you a lot in spotting errors that would otherwise go unnoticed, especially aesthetic errors and broken links.


<a name="file-naming-conventions">

### How should files be named?
Please adhere to [kebab-Case](https://en.wikipedia.org/wiki/Kebab_case) naming rules for any files in Codeberg Documentation.


### When is my contribution being approved?
A Pull Request to Codeberg/Documentation will be reviewed by at least two other Codeberg Documentation collaborators and if both approve of your Pull Request, they will merge it into master.

There are a number of exceptions to the rule above:
- For bugfixes and other small and non-controversial changes, the process is much simplified - here it is enough for one collaborator to green-light and merge the changes. If you're already a collaborator yourself, feel free to merge right away (after sufficient testing, that is).
- For changes that are critical, e.g. ones that might legally affect Codeberg e.V., the chairperson of Codeberg e.V. will need to approve of the changes as well

### Where to create example repositories?
Please create example repositories for the Documentation in the `knut` organization on `codeberg.org`.

If you do not have permissions for that, feel free to ask [in the issue tracker](https://codeberg.org/Codeberg/Documentation/issues).

### How to make screenshots for Codeberg Documentation?
Screenshots for Codeberg Documentation should follow the guidelines explained in
[Kesi Parker's excellent article](https://medium.com/technical-writing-is-easy/screenshots-in-documentation-27b45342aad8).

They should be created using the "Knut" demo account on `codeberg-test.org`. If you're a regular, trusted contributor to Documentation, you can find its credentials in a private repository in the `knut` organization on `codeberg.org`.

Otherwise, feel free to request screenshots being added when merging your PR. For that, please include `TODO SCREENSHOT [of foo]` in your article.

On the technical side, screenshots should ideally be available in both the webP
and PNG formats (webP and JPEG for photographs), they should be in a large enough resolution so that text is clearly
readable and their filenames should follow Codeberg Documentation's [naming conventions](#file-naming-conventions).

Please put screenshots under `assets/images/[section]/[article]/...` where `[section]` and `[article]` are the kebab-cased names of the section or your article, respectively.

Codeberg Documentation uses the webP format wherever possible, in order to increase page loading speed and reduce traffic due to its powerful compression.

To convert a screenshot to webP, you can use the `cwebp` command line utility like this:

```bash
cwebp -lossless screenshot.png -o screenshot.webp
```

If you prefer a GUI, there a [a number of options](https://en.wikipedia.org/wiki/WebP#Graphics_software) available, but please be sure that your GUI doesn't add thumbnails into the WebP file, which would inflate its size, rendering its key advantage meaningless.

> At the time of writing, there is no support for webP images in Safari browsers.
> As a workaround, all images (except SVG) in codeberg documentation are currently to be included using a `<picture>` tag, like so:
>
> ```html
> <picture>
>   <source srcset="/assets/images/getting-started/issue-tracking-basics/issues-list.webp" type="image/webp">
>   <img src="/assets/images/getting-started/issue-tracking-basics/issues-list.png" alt="Issues List">
> </picture>
> ```

### How to link to non-existing articles? / Draft Articles
While it is not directly possible to link to non-existing articles, it is possible to create Draft Articles, which are articles that cannot be localized for example via the menu, but which do exist under their URL and do show a message stating that the article is unpublished.

Creating such a draft article can be done by creating a new article with the
`draft` property set to true, like this:

```yaml
---
eleventyNavigation:
  key: DocsContributorFAQ
  title: Documentation Contributor FAQ
  parent: ImprovingCodeberg
  draft: true
---
```

---
eleventyNavigation:
  key: ContributingCode
  title: Contributing Code
  parent: ImprovingCodeberg
---

Hey there, thank you for heading here and your interest in improving Codeberg. This page is intended to give you a quick start on how and where to contribute code to the platform of Codeberg itself. Some things are always changing and we're not (yet) good at continously updating the docs on that - but we're always glad to help you with your questions. Just reach out and ask!
Since Codeberg uses Gitea as a base, most features that go beyond configuration changes are required upstream and every helping hand is very welcome to improve Gitea and thus Codeberg.

Gitea itself is written in [Go](https://en.wikipedia.org/wiki/Go_(programming_language)), so it's best to make yourself familiar with this language first. It's focused on being a very easy and maintainable language, so it should not be a big issue if you haven't heard of it yet.

## Where do patches go?

We currently sort patches on the following criteria:
- changes in deployments and config goes to the [Codeberg/build-deploy-gitea](https://codeberg.org/Codeberg/build-deploy-gitea) repo. It holds the scripts used to set up the Codeberg instance and overrides other changes, so have a look at it in any case!
- changes that benefit the general gitea userbase should go into the [upsteram repo](https://github.com/go-gitea/gitea). Contributions there also help improving Codeberg, so consider raising a merge request there to help all instances of Gitea
- a small set of patches is directly committed into the [Codeberg/gitea](https://codeberg.org/Codeberg/gitea) repo, these include
  - (non-configurable) template changes or small tweaks that are not intended for upstream
  - hot-fixes for urgent issues (like anti-spam measures) that are sometimes low quality and thus not upstream, but would require a proper solution in the software itself
  - backports of gitea commits with important features or fixes that won't be officially backported
  - cherry-picked commits of pending upstream PRs we want to include sooner

## Getting started

So let's put our hands-on and start hacking. If you don't already have an issue in mind you want to fix, have a look at the [Community issues](https://codeberg.org/Codeberg/Community/issues) and especially those that are labeled with "[contribution welcome](https://codeberg.org/Codeberg/Community/issues?q=&type=all&state=open&labels=105&milestone=0&assignee=0)", this indicates we would really appreciate your help there 🤗. If you didn't find something yet, go for the Gitea issue tracker. It's likely that you find something interesting amongst the thousands of open reports and feature requests there, espcially with the good first issues.

### Working with the `build-deploy-gitea` repo

The deployment of Codeberg is handled via the [Codeberg/build-deploy-gitea](https://codeberg.org/Codeberg/build-deploy-gitea) repository which also contains the code of the Codeberg pages feature. While there's some work on different deployment approaches (more to that later), this is the way stuff is handled as of today.
The deployment Makefile connects to a remote server via ssh, so we recommend configuring a virtual machine for this. You can use the provided script or perform the following tasks inside your favourite VM / container solution:
- add a user and group git
- provide SSH to root
- setup a MySQL database `gitea` end enter the credentials into your app.ini
- install at least git make nodejs npm openssh-server librsvg2-bin (example package names from Debian Buster)
- run the makefile and see that Gitea is installed, you can override variables and run it like `make HOSTNAME_FQDN=localhost deployment`
- after Gitea is installed, you might want to create an admin user for testing (should otherwise be first user by default), you can use `sudo -u git GITEA_WORK_DIR=/data/git GITEA_CUSTOM/etc/gitea /data/git/bin/gitea admin create-user --username you --admin --password yourpassword --email youremail@example.com`
Please see the deployment repo for the folder structure as well as the [upstream docs](https://docs.gitea.io/en-us/) on how to configure and hack Gitea.

Also note, that the Makefile by defaults compiles Codeberg's Gitea fork from the `codeberg-test` branch. You can override this behaviour by specifying ORIGIN and GITEA_BRANCH environment to the Makefile.

### Working with the gitea codebase

Please look at [the Gitea docs](https://docs.gitea.io/en-us/hacking-on-gitea/) on how to start hacking on Gitea itself. They provide more detailed and especially up-to-date information than we could maintain in our own docs. Additionally, there's probably no need for reduncandy here 😉.

### Raising an MR to our Gitea fork

If you fixed an issue specific to Codeberg that should not go into the upstream repo or was refused there, but fits the criteria for our own repo written above, you can file an MR to [our own fork of Gitea](https://codeberg.org/Codeberg/gitea). Make sure to point your MR to the codeberg branch and rebase to the latest commits there. Also explain, why this should go into the Codeberg repo, if not obvious.

> **Warning** Please note, that the branches may be force-pushed at 
> certain times to make sure the Codeberg commits are well visible and 
> maintainable at any time. This may lead to confusing behaviour of the 
> merge requests, i. e. they seem to include commits you haven't made.
> 
> If you face this issue, you can `git rebase` your commits onto the 
> current Codeberg branch and force-push. Use
> ```
> git rebase origin/codeberg
> git push -f
> ```
> for example.

### Working on better deployment methods

If you want to help us improving our deployment scripts, check out the [Codeberg-Infrastructure organization](https://codeberg.org/Codeberg-Infrastructure) (only visible when logged in) and check out eventual configuration. We'd really appreciate to move away from Makefile and shell scripts and ask you to join the discussion there. Please make sure to first understand what the `build-deploy-gitea` repo does to replicate it's function. Also feel free to contact us (for example via Matrix) to ask where to help.

## Still questions?

If you need help or feel stuck, need inspiration or advice, go ahead and ask the community. If you picked up an issue somewhere, feel free to ask your question there. Also, you can always ask in the Matrix channel at [#codeberg@matrix.org](https://matrix.to/#/#codeberg.org:matrix.org).

Thank you very much for your interest in improving Codeberg.

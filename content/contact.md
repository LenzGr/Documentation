---
eleventyNavigation:
  key: Contact
  title: Contact
  icon: envelope
  order: 90
---

## Questions and Issues
If you have questions or found any issues with Codeberg, please create an [issue on Codeberg/Community](https://codeberg.org/Codeberg/Community/issues).

## Email
For direct contact, you can write an email to [contact@codeberg.org](mailto:contact@codeberg.org).

## Legal inquiries
For legal inquiries, please refer to the [Imprint](https://codeberg.org/codeberg/org/src/branch/master/Imprint.md).
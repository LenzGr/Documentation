function getSidebar() {
    return document.querySelector("#sidebar")
}

function getSidebarOverlay() {
    return document.querySelector("#sidebar-overlay")
}

function hideSidebar() {
    resetSidebarAnimation()
    getSidebar().style.animation = "slideSidebar 0.5s ease-in-out reverse backwards"
}

function showSidebar() {
    resetSidebarAnimation()

    const sidebar = getSidebar()
    sidebar.classList.add("sidebar-in")
    sidebar.style.animation = "slideSidebar 0.5s ease-in-out normal forwards"

    const overlay = getSidebarOverlay()
    overlay.classList.add("sidebar-overlay-in")
    overlay.style.animation = "fadeOverlay 0.5s ease-in-out normal forwards"
}

function resetSidebarAnimation() {
    const sidebar = getSidebar()
    sidebar.classList.remove("sidebar-in")
    sidebar.style.animation = "none"
    void sidebar.offsetWidth // Needed to restart animation

    const overlay = getSidebarOverlay()
    overlay.classList.remove("sidebar-overlay-in")
    overlay.style.animation = "none"
    void overlay.offsetWidth // Needed to restart animation
}

function toggleSidebar() {
    if (getSidebar().classList.contains("sidebar-in"))
        hideSidebar()
    else
        showSidebar()
}

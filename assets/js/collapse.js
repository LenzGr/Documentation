function onCollapseButtonClick(button) {
    let icon = button.querySelector(".fas")
    
    let collapseId = button.dataset["collapseId"]
    let collapseElement = document.getElementById(collapseId)
    if (collapseElement.style.maxHeight == "0px") {
        icon.classList.remove("fa-plus-square")
        icon.classList.add("fa-minus-square")
        collapseElement.style.maxHeight = collapseElement.scrollHeight + "px"
    } else {
        console.log(collapseElement.style.maxHeight)
        if (collapseElement.style.maxHeight === "") { // This is a workaround to fix animation when no initial maxHeight has been set
            collapseElement.style.maxHeight = collapseElement.scrollHeight + "px"
            void collapseElement.offsetWidth // Needed to trigger render
        }


        icon.classList.add("fa-plus-square")
        icon.classList.remove("fa-minus-square")
        collapseElement.style.maxHeight = "0px"
    }
}
